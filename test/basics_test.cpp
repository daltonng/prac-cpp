//
// Created by Thong D. Nguyen on 6/23/20.
//

#include <gtest/gtest.h>
#include <basics.h>

TEST(basicsTest, HandlesZeroInput) {
    EXPECT_EQ(1, 1);
}