//
// Created by Thong D. Nguyen on 6/23/20.
//

#include <basics.h>
#include <iostream>
#include <typeinfo>

int auto_variables() {
    char c { 'T' };
    bool b { true };
    int i { 36 };
    double d { 17.29 };
    auto a { 1.5 };
    std::cout << "Evaluated at runtime " << a << typeid(a).name() << std::endl;

    return 0;
}

int constants() {
    int i = 1983;
    const int ci = 37;
    const int cri = i + ci;
    constexpr int cci = 1983 + ci;  // constexpr must be constant at compiled time
    std::cout << "Evaluated at compiled time " << cci << typeid(cci).name() << std::endl;

    return 0;
}